package jdolanski.tvz.botnet.candc.form;

import jdolanski.tvz.botnet.candc.model.Command;

import java.util.List;

/**
 * Created by jdolanski on 26.12.16..
 */
public class BotsCommandsForm {
    private List<String> ids;
    private List<Command> commands;

    public List<String> getIds() {
        return ids;
    }

    public void setIds(List<String> ids) {
        this.ids = ids;
    }

    public List<Command> getCommands() {
        return commands;
    }

    public void setCommands(List<Command> commands) {
        this.commands = commands;
    }
}
