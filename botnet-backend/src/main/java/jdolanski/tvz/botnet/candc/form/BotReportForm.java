package jdolanski.tvz.botnet.candc.form;

import jdolanski.tvz.botnet.candc.model.BotStatus;

import java.io.Serializable;

/**
 * Created by admin on 21.12.2016..
 */
public class BotReportForm implements Serializable {
    private String id;

    private String secret;

    private String bot_uuid;

    private BotStatus status;

    private Integer ramUsage;

    private String commandResult;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getBot_uuid() {
        return bot_uuid;
    }

    public void setBot_uuid(String bot_uuid) {
        this.bot_uuid = bot_uuid;
    }

    public Integer getRamUsage() {
        return ramUsage;
    }

    public void setRamUsage(Integer ramUsage) {
        this.ramUsage = ramUsage;
    }

    public BotStatus getStatus() {
        return status;
    }

    public void setStatus(BotStatus status) {
        this.status = status;
    }

    public String getCommandResult() {
        return commandResult;
    }

    public void setCommandResult(String commandResult) {
        this.commandResult = commandResult;
    }
}
