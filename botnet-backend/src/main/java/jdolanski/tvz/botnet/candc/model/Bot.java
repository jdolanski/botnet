package jdolanski.tvz.botnet.candc.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * Created by admin on 20.12.2016..
 */
@Entity
@Table(name = "bot")
public class Bot implements Serializable {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    @Column(name = "id")
    private UUID id;

    @Column(name = "uuid")
    private String bot_uuid;

    @Column(name = "ipv4")
    private String ip;

    @Column(name = "port")
    private Integer port;

    @Column(name = "country")
    private String country;

    @Column(name = "ram")
    private Integer ram;

    @Column(name = "seen")
    private LocalDateTime lastSeen;

    @Column(name = "os")
    private String os;

    @Column(name = "status")
    private BotStatus status;

    @Column(name = "cmd_report")
    @Lob @Basic(fetch = FetchType.LAZY)
    private String commandResult;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "bot_cmd_mapping",
            joinColumns = {@JoinColumn(name = "bot_id", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "cmd_id", nullable = false, updatable = false)}
    )
    private List<Command> commandList;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getRam() {
        return ram;
    }

    public void setRam(Integer ram) {
        this.ram = ram;
    }

    public LocalDateTime getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(LocalDateTime lastSeen) {
        this.lastSeen = lastSeen;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public BotStatus getStatus() {
        return status;
    }

    public void setStatus(BotStatus status) {
        this.status = status;
    }

    public List<Command> getCommandList() {
        return commandList;
    }

    public void setCommandList(List<Command> commandList) {
        this.commandList = commandList;
    }

    public String getBot_uuid() {
        return bot_uuid;
    }

    public void setBot_uuid(String bot_uuid) {
        this.bot_uuid = bot_uuid;
    }

    public String getCommandResult() {
        return commandResult;
    }

    public void setCommandResult(String commandResult) {
        this.commandResult = commandResult;
    }
}
