package jdolanski.tvz.botnet.candc.form.validator;


import jdolanski.tvz.botnet.candc.form.BotReportForm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by jdolanski on 26.12.16..
 */
@Component
public class BotReportFormValidator implements Validator {
    @Value("${app.secret}")
    private String secret;

    @Override
    public boolean supports(Class<?> clazz) {
        return BotReportForm.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        final BotReportForm form = (BotReportForm) target;

        if (form.getId() == null)
            errors.reject("no_id");

        if (! StringUtils.hasText(form.getSecret()))
            errors.reject("no_secret");
        else if (! form.getSecret().equals(this.secret))
            errors.reject("invalid_secret");

        if (! StringUtils.hasText(form.getBot_uuid()))
            errors.reject("no_uuid");

        if (form.getStatus() == null)
            errors.reject("no_status");

        if (form.getCommandResult() == null)
            errors.reject("no_exit-code");
    }
}
