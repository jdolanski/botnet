package jdolanski.tvz.botnet.candc.jpa;

import jdolanski.tvz.botnet.candc.model.Bot;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * Created by admin on 21.12.2016..
 */
public interface BotRepository extends JpaRepository<Bot, UUID> {
}
