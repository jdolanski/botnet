package jdolanski.tvz.botnet.candc.form;

/**
 * Created by jdolanski on 26.12.16..
 */
public class BotRegistrationForm {
    private String os;
    private String secret;
    private String bot_uuid;

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getBot_uuid() {
        return bot_uuid;
    }

    public void setBot_uuid(String bot_uuid) {
        this.bot_uuid = bot_uuid;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }
}
