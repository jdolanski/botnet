package jdolanski.tvz.botnet.candc.scheduled;

import jdolanski.tvz.botnet.candc.jpa.BotRepository;
import jdolanski.tvz.botnet.candc.model.Bot;
import jdolanski.tvz.botnet.candc.model.BotStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by admin on 18.1.2017..
 */
@Component
public class ScheduledBotCheck {
    private static final Logger log = LoggerFactory.getLogger(ScheduledBotCheck.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    private final BotRepository botRepository;

    @Autowired
    public ScheduledBotCheck(BotRepository botRepository) {
        this.botRepository = botRepository;
    }

    @Scheduled(fixedRate = 30000)
    public void reportCurrentTime() {
        List<Bot> inactiveBots = botRepository.findAll()
                .stream()
                .filter(bot ->
                        LocalDateTime.now().minusMinutes(10)
                                .isAfter(bot.getLastSeen())
                )
                .collect(Collectors.toList());
        inactiveBots.forEach(bot -> bot.setStatus(BotStatus.INACTIVE));
        botRepository.save(inactiveBots);

        log.info("[{}] There are {} inactive bots",
                dateFormat.format(new Date()), inactiveBots.size());
    }
}
