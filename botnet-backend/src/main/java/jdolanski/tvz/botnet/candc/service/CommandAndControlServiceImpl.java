package jdolanski.tvz.botnet.candc.service;

import jdolanski.tvz.botnet.candc.dto.BotDetailsDto;
import jdolanski.tvz.botnet.candc.dto.BotDto;
import jdolanski.tvz.botnet.candc.jpa.BotRepository;
import jdolanski.tvz.botnet.candc.model.Bot;
import jdolanski.tvz.botnet.candc.model.BotStatus;
import jdolanski.tvz.botnet.candc.model.Command;
import jdolanski.tvz.botnet.candc.model.CommandType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Created by jdolanski on 25.12.16..
 */
@Service
public class CommandAndControlServiceImpl implements CommandAndControlService {
    private final BotRepository botRepository;

    @Autowired
    public CommandAndControlServiceImpl(BotRepository botRepository) {
        this.botRepository = botRepository;
    }

    @Override
    public List<BotDto> getAll() {
        return botRepository.findAll().parallelStream()
                .map(BotDto::mapToDto).collect(Collectors.toList());
    }

    @Override
    public BotDetailsDto getById(String id) {
        return BotDetailsDto.mapToDto(botRepository.findOne(UUID.fromString(id)));
    }

    @Transactional
    @Override
    public void killBot(String id) {
        botRepository.delete(UUID.fromString(id));
    }

    @Override
    public CommandType[] getCommandTypes() {
        return CommandType.values();
    }

    @Transactional
    @Override
    public void pushCommandsToBot(String id, List<Command> commands) {
        Bot bot = botRepository.findOne(UUID.fromString(id));

        if (bot != null) {
            bot.getCommandList().addAll(commands);
            botRepository.save(bot);
        }
    }

    @Transactional
    @Override
    public void pushCommandsToBotList(List<String> ids, List<Command> commands) {
        List<UUID> uuids = ids.parallelStream()
                .map(UUID::fromString).collect(Collectors.toList());
        List<Bot> bots = botRepository.findAll(uuids);

        if (bots.size() == uuids.size()) {
            bots.forEach(bot -> bot.getCommandList().addAll(commands));
            botRepository.save(bots);
        }
    }

    @Transactional
    @Override
    public void pushCommandsToAllBots(List<Command> commands) {
        List<Bot> bots = botRepository.findAll();

        bots.forEach(bot -> bot.getCommandList().addAll(commands));
        botRepository.save(bots);
    }

    @Transactional
    @Override
    public Integer markInactiveBots(Long minutes) {
        List<Bot> inactiveBots = botRepository.findAll().parallelStream()
                .filter(bot -> LocalDateTime.now().minusMinutes(minutes).isAfter(bot.getLastSeen()))
                .collect(Collectors.toList());
        inactiveBots.forEach(bot -> bot.setStatus(BotStatus.INACTIVE));

        return botRepository.save(inactiveBots).size();
    }
}
