package jdolanski.tvz.botnet.candc.dto;

import jdolanski.tvz.botnet.candc.model.Command;
import jdolanski.tvz.botnet.candc.model.CommandType;

/**
 * Created by admin on 12.12.2016..
 */
public class CommandDto {
    private CommandType type;
    private String content;


    public static CommandDto mapToDto(Command command) {
        CommandDto commandDto = new CommandDto();

        commandDto.setType(command.getType());
        commandDto.setContent(command.getContent());

        return commandDto;
    }


    public CommandType getType() {
        return type;
    }

    public void setType(CommandType type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
