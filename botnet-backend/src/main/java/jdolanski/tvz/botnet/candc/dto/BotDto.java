package jdolanski.tvz.botnet.candc.dto;

import jdolanski.tvz.botnet.candc.model.Bot;
import jdolanski.tvz.botnet.candc.model.BotStatus;

/**
 * Created by jdolanski on 26.12.16..
 */
public class BotDto {
    private String id;
    private String ip;
    private Integer port;
    private Integer ram;
    private String lastSeen;
    private String os;
    private BotStatus status;
    private String commandResult;


    public static BotDto mapToDto(Bot bot) {
        BotDto botDto = new BotDto();

        botDto.setId(bot.getId().toString());
        botDto.setIp(bot.getIp());
        botDto.setPort(bot.getPort());
        botDto.setRam(bot.getRam());
        botDto.setLastSeen(bot.getLastSeen() != null ? bot.getLastSeen().toString() : null);
        botDto.setOs(bot.getOs());
        botDto.setStatus(bot.getStatus());
        botDto.setCommandResult(bot.getCommandResult());

        return botDto;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Integer getRam() {
        return ram;
    }

    public void setRam(Integer ram) {
        this.ram = ram;
    }

    public String getLastSeen() {
        return lastSeen;
    }

    public void setLastSeen(String lastSeen) {
        this.lastSeen = lastSeen;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public BotStatus getStatus() {
        return status;
    }

    public void setStatus(BotStatus status) {
        this.status = status;
    }

    public String getCommandResult() {
        return commandResult;
    }

    public void setCommandResult(String commandResult) {
        this.commandResult = commandResult;
    }
}
