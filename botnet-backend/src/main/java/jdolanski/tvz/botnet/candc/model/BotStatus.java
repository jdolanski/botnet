package jdolanski.tvz.botnet.candc.model;

/**
 * Created by admin on 7.1.2017..
 */
public enum BotStatus {
    INIT(0),
    LISTEN(1),
    EXECUTING(2),
    INACTIVE(3);

    private final Integer status;

    private BotStatus(Integer status) {
        this.status = status;
    }

    public Integer getStatus() {
        return status;
    }
}
