package jdolanski.tvz.botnet.candc.model;

/**
 * Created by admin on 7.1.2017..
 */
public enum CommandType {
    KILL(0),
    WAIT(1),
    DOS(2),
    RAW(3),
    NOTHING(4);

    private final Integer type;

    private CommandType(Integer type) {
        this.type = type;
    }

    public Integer getType() {
        return type;
    }
}
