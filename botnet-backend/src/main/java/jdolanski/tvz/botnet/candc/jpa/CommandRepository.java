package jdolanski.tvz.botnet.candc.jpa;

import jdolanski.tvz.botnet.candc.model.Command;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by jdolanski on 26.12.16..
 */
public interface CommandRepository extends JpaRepository<Command, Long> {
}
