package jdolanski.tvz.botnet.candc.service;

import jdolanski.tvz.botnet.candc.dto.BotDetailsDto;
import jdolanski.tvz.botnet.candc.dto.BotDto;
import jdolanski.tvz.botnet.candc.model.Command;
import jdolanski.tvz.botnet.candc.model.CommandType;

import java.util.List;

/**
 * Created by jdolanski on 25.12.16..
 */
public interface CommandAndControlService {
    List<BotDto> getAll();

    BotDetailsDto getById(String id);

    void killBot(String id);

    CommandType[] getCommandTypes();

    void pushCommandsToBot(String id, List<Command> commands);

    void pushCommandsToBotList(List<String> ids, List<Command> commands);

    void pushCommandsToAllBots(List<Command> commands);

    Integer markInactiveBots(Long minutes);
}
