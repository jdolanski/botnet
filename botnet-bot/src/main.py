#!/usr/bin/env python3

from bot import Bot


def main():
    bot = Bot()
    bot.boot()
    if bot.bot_id != '0':
        print('Bot ID: {}. Bot MAC ADDRESS: {}'.format(bot.bot_id, bot.bot_uuid))
        bot.communicate_with_cc()
    else:
        print('Error!')


if __name__ == '__main__':
    main()
