#!/usr/bin/env python3
from enum import Enum
from collections import namedtuple


class CommandEnum(Enum):
    KILL = 0
    WAIT = 1
    DOS = 2
    RAW = 3,
    NOTHING = 4


Command = namedtuple('Command', 'type content')


def convert_dict_to_command(dictionary) -> Command:
    return Command(type=dictionary['type'], content=dictionary['content'])
