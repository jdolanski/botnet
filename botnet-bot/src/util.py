#!/usr/bin/env python3

import json
from itertools import starmap, cycle


def to_json(object):
    return json.dumps(object.__dict__)


def memory_usage_resource():
    pass