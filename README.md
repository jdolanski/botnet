# README #
* botnet-backend: Java REST server @ localhost:8081
* botnet-frontend: Javascript web app (optional) @ localhost:4200
* botnet-bot: Python 3 bot script

### Required:
* Java 8 (for back-end)
* NPM 3 (for front-end)
* Python 3 (for bot)


### Running
botnet-backend
``` bash
# UNIX:
./gradlew :botnet-backend:bootrun
```
``` bash
# WINDOWS:
gradlew.bat :botnet-backend:bootrun
```

botnet-frontend
``` bash
cd botnet-frontend
# First install packages
npm i
# Run
npm run start
```

botnet-bot
``` bash
# UNIX:
cd botnet-bot/src
python3 main.py
```
``` bash
# WINDOWS:
cd botnet-bot\src
python main.py
```

### Example
![Capture.JPG](https://bitbucket.org/repo/jendEo/images/3044251079-Capture.JPG)