import {CommandDto} from "./command.dto";
/**
 * Created by admin on 28.12.2016..
 */
export interface BotDetailsDto {
  id: string;
  ip: string;
  port: number;
  ram: number;
  lastSeen: string;
  os: string;
  status: string;
  commandResult: string;
  commands: CommandDto[];
}
