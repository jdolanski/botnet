/**
 * Created by admin on 29.12.2016..
 */
export interface CommandDto {
  type: number;
  content: string;
}
