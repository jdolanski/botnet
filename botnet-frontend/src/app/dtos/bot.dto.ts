/**
 * Created by admin on 28.12.2016..
 */
export class BotDto {
  id: string;
  ip: string;
  port: number;
  ram: number;
  lastSeen: string;
  os: string;
  status: string;
}
