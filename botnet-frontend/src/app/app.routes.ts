import {Routes} from "@angular/router";
import {HomeComponent} from "./home/home.component";
import {BotDetailsComponent} from "./bot-details/bot-details.component";

/**
 * Created by admin on 19.12.2016..
 */
export const MyRoutes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  { path: 'bot', pathMatch: 'full', redirectTo: 'home' },
  { path: 'home', component: HomeComponent },
  { path: 'bot/:id', component: BotDetailsComponent }
];

export const RoutedComponents = [
  HomeComponent,
  BotDetailsComponent
];
