import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {AppComponent} from "./app.component";
import {RouterModule} from "@angular/router";
import {MyRoutes, RoutedComponents} from "./app.routes";
import {HttpWrapperService} from "./http-wrapper.service";
import {CommandsComponent} from "./commands/commands.component";
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {AddCommandsToBotComponent} from "./add-commands-to-bot/add-commands-to-bot.component";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgbModule.forRoot(),
    ReactiveFormsModule,
    RouterModule.forRoot(MyRoutes)
  ],
  declarations: [
    AppComponent,
    CommandsComponent,
    AddCommandsToBotComponent,
    RoutedComponents
  ],
  providers: [HttpWrapperService],
  bootstrap: [AppComponent]
})
export class AppModule { }
