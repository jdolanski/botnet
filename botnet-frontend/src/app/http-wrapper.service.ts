import {Injectable} from "@angular/core";
import {Http, RequestOptionsArgs, Response} from "@angular/http";
import {Observable} from "rxjs";

/**
 * Created by admin on 19.12.2016..
 */
@Injectable()
export class HttpWrapperService {

  constructor(private http: Http) {

  }

  public get(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.http.get(url, options);
  }
  post(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return this.http.post(url, body, options);
  }
  put(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return this.http.put(url, body, options);
  }
  delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.http.delete(url, options);
  }
  patch(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return this.http.patch(url, body, options);
  }
  head(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.http.head(url, options);
  }
  options(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.http.options(url, options);
  }
}
