/**
 * Created by admin on 28.12.2016..
 */
import {Component, Input} from "@angular/core";
import {CommandDto} from "../dtos/command.dto";

@Component({
  selector: 'app-commands',
  templateUrl: './commands.component.html'
})
export class CommandsComponent {
  @Input() commands: CommandDto[];

  constructor() {

  }
}
