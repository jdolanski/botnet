import {Component, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {HttpWrapperService} from "../http-wrapper.service";
import {BotDetailsDto} from "../dtos/bot-details.dto";
import {environment} from "../../environments/environment";
import {CommandDto} from "../dtos/command.dto";
import {Headers} from "@angular/http";
import {Observable} from "rxjs";

@Component({
  selector: 'app-bot-details',
  templateUrl: './bot-details.component.html',
  styleUrls: ['./bot-details.component.css']
})
export class BotDetailsComponent implements OnInit {
  botDetails: BotDetailsDto;
  spinRefresh: boolean = false;
  refetchDisabled: boolean = false;
  addCommand: boolean = false;
  commandTypes: string[];
  datetimeFormat: string = environment.datetime_format;

  constructor(private route: ActivatedRoute,
              private http: HttpWrapperService) {
  }

  ngOnInit() {
    let botId: string = this.route.snapshot.params['id'];
    this.getBotDetails(botId).subscribe(response => this.botDetails = response.json() as BotDetailsDto);
    this.getCommandTypes().subscribe(response => this.commandTypes = response.json() as string[]);
  }

  refetchDetails(): void {
    this.refetchDisabled = true;
    this.spinRefresh = true;
    let botObservable = this.getBotDetails(this.botDetails.id).map(res => res.json() as BotDetailsDto);
    let typesObservable = this.getCommandTypes().map(res => res.json() as string[]);

    Observable.forkJoin([botObservable, typesObservable])
      .subscribe(data => {
        this.botDetails = data[0];
        this.commandTypes = data[1];
        console.log(this.commandTypes);

        setTimeout(() => {
          this.spinRefresh = false;
          this.refetchDisabled = false;
        }, 1000);
      });
  }

  openAddCommandForm(event: Event): void {
    this.addCommand = true;
  }

  closeAddCommandForm(): void {
    this.addCommand = false;
  }

  onSearchFormSubmit(command: CommandDto): void {
    if (command != null) {
      this.http.post(`${environment.api_endpoint}/api/v1/candc/cmd/${this.botDetails.id}`,
        [command], {headers: new Headers({'Content-Type': 'application/json'})}
      ).subscribe(() => {
        this.closeAddCommandForm();
        this.refetchDetails();
      });
    } else {
      this.closeAddCommandForm();
    }
  }

  private getBotDetails(botId: string): Observable<any> {
    return this.http.get(`${environment.api_endpoint}/api/v1/candc/${botId}`);
  }

  private getCommandTypes(): Observable<any> {
    return this.http.get(`${environment.api_endpoint}/api/v1/candc/cmd/types`);
  }
}
