import {Component, OnInit} from "@angular/core";
import {HttpWrapperService} from "../http-wrapper.service";
import {environment} from "../../environments/environment";
import {BotDto} from "../dtos/bot.dto";
import {Observable} from "rxjs";
import {Headers} from "@angular/http";
import {CommandDto} from "../dtos/command.dto";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  bots: BotDto[];
  spinRefresh: boolean = false;
  refetchDisabled: boolean = false;
  addCommand: boolean = false;
  commandTypes: string[];
  datetimeFormat: string = environment.datetime_format;

  constructor(private http: HttpWrapperService) { }

  ngOnInit() {
    this.getBots().subscribe(response => this.bots = response.json() as BotDto[]);
    this.getCommandTypes().subscribe(response => this.commandTypes = response.json() as string[]);
  }

  refetchData(): void {
    this.refetchDisabled = true;
    this.spinRefresh = true;
    let botObservable = this.getBots().map(res => res.json() as BotDto[]);
    let typesObservable = this.getCommandTypes().map(res => res.json() as string[]);

    Observable.forkJoin([botObservable, typesObservable])
      .subscribe(data => {
        this.bots = data[0];
        this.commandTypes = data[1];
        console.log(this.commandTypes);
        setTimeout(() => {
          this.spinRefresh = false;
          this.refetchDisabled = false;
        }, 1000);
      });
  }

  openAddCommandForm(event: Event): void {
    this.addCommand = true;
  }

  closeAddCommandForm(): void {
    this.addCommand = false;
  }

  onSearchFormSubmit(command: CommandDto): void {
    if (command != null) {
      this.http.post(`${environment.api_endpoint}/api/v1/candc/cmd/all`,
        [command], {headers: new Headers({'Content-Type': 'application/json'})}
      ).subscribe(() => {
        this.closeAddCommandForm();
        this.refetchData();
      });
    } else {
      this.closeAddCommandForm();
    }
  }

  killBot(botId: number): void {
    this.http.delete(`${environment.api_endpoint}/api/v1/candc/${botId}`)
      .subscribe(() => this.refetchData());
  }

  private getBots(): Observable<any> {
    return this.http.get(`${environment.api_endpoint}/api/v1/candc/all`);
  }

  private getCommandTypes(): Observable<any> {
    return this.http.get(`${environment.api_endpoint}/api/v1/candc/cmd/types`);
  }
}
