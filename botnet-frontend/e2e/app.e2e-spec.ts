import { BotnetFrontendPage } from './app.po';

describe('botnet-frontend App', function() {
  let page: BotnetFrontendPage;

  beforeEach(() => {
    page = new BotnetFrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
